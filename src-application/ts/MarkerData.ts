import L, { Icon } from "leaflet";

export { MarkerData };


var CustomMarkerIcon = L.Icon.extend({
    options: {
        shadowUrl: 'images/marker-shadow.png',
        iconSize:     [25, 40],  // size of the icon
        shadowSize:   [41, 41],  // size of the shadow
        iconAnchor:   [12, 40],  // point of the icon which will correspond to marker's location
        shadowAnchor: [12, 40],  // the same for the shadow
        popupAnchor:  [0, -34]   // point from which the popup should open relative to the iconAnchor
    }
});

class MarkerData {

    /** Heading shown at the top of the marker popup */
    public heading: string;
    /** Coordinates of the marker */
    public coordinates: [number, number];
    /** Main content, used by overview popups */
    public popupContentMain: DocumentFragment;
    /** Persons born in that place */
    public popupContentBirths: DocumentFragment;
    /** People who died there */
    public popupContentDeaths: DocumentFragment;
    /** List of submissions coming from that place */
    public popupContentSubmissions: DocumentFragment;
    /** Whether the popup contains birth information */
    public containsBirth: number;
    /** Whether the popup contains death information */
    public containsDeath: number;
    /** Whether the popup contains submission information */
    public containsSubmissions: number;
    /** Leaflet icon for the map */
    public icon: Icon;
    public overviewTooltipText: string;

    constructor() {

        this.popupContentMain = document.createDocumentFragment();
        this.popupContentBirths = document.createDocumentFragment();
        this.popupContentDeaths = document.createDocumentFragment();
        this.popupContentSubmissions = document.createDocumentFragment();
        this.containsBirth = 0;
        this.containsDeath = 0;
        this.containsSubmissions = 0;
        this.overviewTooltipText = "";

    }

    /**
     * Creates an icon depending on the contents of the popup
     */
    createIcon() {

        let countTypes = 0;
        if (this.containsBirth) countTypes +=1;
        if (this.containsDeath) countTypes +=1;
        if (this.containsSubmissions) countTypes +=1;

        if (countTypes > 1) {
            //@ts-ignore (following Leaflet docs)
            this.icon = new CustomMarkerIcon({iconUrl: 'images/marker-icon-ellipsis.png'});
        } else if (this.containsBirth) {
            //@ts-ignore
            this.icon = new CustomMarkerIcon({iconUrl: 'images/marker-icon-asterisk.png'});
        } else if (this.containsDeath) {
            //@ts-ignore
            this.icon = new CustomMarkerIcon({iconUrl: 'images/marker-icon-dagger.png'});
        }
    }

}