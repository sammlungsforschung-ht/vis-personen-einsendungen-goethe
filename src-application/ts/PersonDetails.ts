import { customElementManager, goeMapApp } from "./main";
import { personData } from "./personData";

export { PersonDetails };


/**
 * Provides functionality for the person details sidebar (de: Steckbrief).
 */
class PersonDetails {

    private wrapperDiv: HTMLDivElement;
    private personDetailsElement: HTMLElement;
    private personDetailsMultipleElement: HTMLElement;

    constructor() {

        this.wrapperDiv = document.getElementById("goemap__person_details") as HTMLDivElement;
        this.wrapperDiv.appendChild(this.createHTML());

    }

    /**
     * Prepares the details section
     */
    createHTML(): DocumentFragment {

        let docFrag = document.createDocumentFragment();

        this.personDetailsElement = customElementManager.createCustomElement("goemap-person-details", {});
        this.personDetailsElement.classList.add("goemap__person_details");
        docFrag.append(this.personDetailsElement);


        let placeholdersMultiple = {
            "num_selected": "0",
            "submission_list": document.createElement("div")
        }
        this.personDetailsMultipleElement = customElementManager.createCustomElement("goemap-person-details-multiple", placeholdersMultiple);
        this.personDetailsMultipleElement.classList.add("goemap__person_details");
        this.personDetailsMultipleElement.classList.add("goemap__person_details--hidden");
        docFrag.append(this.personDetailsMultipleElement);

        // Onclick functions for the overview button
        (this.personDetailsElement.getElementsByClassName("goemap__back_to_overview")[0] as HTMLButtonElement).onclick = () => {
            goeMapApp.showOverview();
        }
        (this.personDetailsMultipleElement.getElementsByClassName("goemap__back_to_overview")[0] as HTMLButtonElement).onclick = () => {
            goeMapApp.showOverview();
        }

        return docFrag;

    }

    /**
     * Wrapper function for updating the details section.
     * Handles visibility of the respective details section (one vs. multiple)
     * @param selection Array of selected GNDs
     */
    updateData(selection: Array<string>) {

        // One person is selected
        if (selection.length == 1) {
            if (!(this.personDetailsMultipleElement.classList.contains("goemap__person_details--hidden"))) {
                this.personDetailsMultipleElement.classList.add("goemap__person_details--hidden")
            }
            if (this.personDetailsElement.classList.contains("goemap__person_details--hidden")) {
                this.personDetailsElement.classList.remove("goemap__person_details--hidden");
            }
            this.updateElement(selection);
        }

        // More than one person is selected
        else if (selection.length > 1) {
            if (!(this.personDetailsElement.classList.contains("goemap__person_details--hidden"))) {
                this.personDetailsElement.classList.add("goemap__person_details--hidden")
            }
            if (this.personDetailsMultipleElement.classList.contains("goemap__person_details--hidden")) {
                this.personDetailsMultipleElement.classList.remove("goemap__person_details--hidden");
            }
            this.updateElementMultiple(selection);
        }
    }

    /**
     * Creates a button that integrates nicely into text. Clicking on it focuses the place on the map.
     * @param textContent Text content of the button
     * @param coordinates Coordinates of the place the button should link to
     * @returns HTMLButtonElement
     */
    createGoToPlaceButton(textContent: string, coordinates: [number, number]): HTMLButtonElement {
        let placeButton = document.createElement("button");
        placeButton.classList.add("goemap__link_button");
        placeButton.textContent = textContent;
        placeButton.onclick = () => {
            goeMapApp.focusMarker(`${coordinates[0]}${coordinates[1]}`);
        }

        return placeButton;
    }

    /**
     * Updates the details section when one person is selected
     * @param selection Array of selected GNDs
     */
    updateElement(selection: Array<string>) {

        let selectedPersonData = personData["Personen"][selection[0]];

        let personImage = document.createElement("img") as HTMLImageElement;
        personImage.alt = `Porträt von ${selectedPersonData["einsender"]}`;

        if (selectedPersonData["bild_link"]) {
            personImage.src = selectedPersonData["bild_link"];

        } else {
            personImage.src = "images/placeholder_portrait_de.png"
        }
        personImage.loading = "lazy";



        let wikidataLink = document.createElement("a");
        wikidataLink.href = selectedPersonData["wikidata_link"];
        wikidataLink.textContent = "Wikidata";

        
        let deutscheBiographieLink = document.createElement("a");
        deutscheBiographieLink.href = selectedPersonData["deutsche_biographie_link"];
        deutscheBiographieLink.textContent = "Biographie auf www.deutsche-biographie.de";

        // Truncates the actual biography text so it does not contain the summary ("read more" functionality)
        let biographyText = selectedPersonData["wikipedia zusammenfassung"];
        let biographySummary = selectedPersonData["wikipedia zusammenfassung"];
        let charIdx = 0;

        // if (biographySummary) {
        //     while (biographySummary[charIdx] === biographyText[charIdx]) {
        //         charIdx += 1;
        //     }
        //     biographyText = biographyText.slice(charIdx, biographyText.length);
        // }


        let buttonPlaceBirth;
        let buttonPlaceDeath;

        if (selectedPersonData["placeOfBirth"]) {
            buttonPlaceBirth = this.createGoToPlaceButton(selectedPersonData["placeOfBirth"]["name"], [selectedPersonData["placeOfBirth"]["latitude"], selectedPersonData["placeOfBirth"]["longitude"]])
        }
        if (selectedPersonData["placeOfDeath"]) {
            buttonPlaceDeath = this.createGoToPlaceButton(selectedPersonData["placeOfDeath"]["name"], [selectedPersonData["placeOfDeath"]["latitude"], selectedPersonData["placeOfDeath"]["longitude"]])    
        }

        let personFunctions = [];
        for (let submissionID of selectedPersonData["Einsendungen"]) {
            
            if (personData["Einsendungen"][submissionID] 
                && "Einsender Funktion" in personData["Einsendungen"][submissionID]
                && !personFunctions.includes(personData["Einsendungen"][submissionID]["Einsender Funktion"])) {
                personFunctions.push(personData["Einsendungen"][submissionID]["Einsender Funktion"]);
            }
        }

        let placeholders = {
            "person_name": selectedPersonData["einsender"],
            "person_img": personImage,
            "submission_count": `${selectedPersonData["Einsendungen"].length}`,
            "string_submission_count": "Anzahl Einsendungen",
            "person_functions": personFunctions.join(", "),
            "string_person_functions": "Funktionen",
            "date_born": selectedPersonData["dateOfBirth"],
            "date_deceased": selectedPersonData["dateOfDeath"],
            "place_birth": buttonPlaceBirth,
            "place_death": buttonPlaceDeath,
            "biography": selectedPersonData["wikipedia zusammenfassung"],
            "biography_short": selectedPersonData["wikipedia zusammenfassung"],
            "deutsche_biographie_link": deutscheBiographieLink,
            "wikidata_link": wikidataLink,
            "submission_list": this.createSubmissionListElement(selectedPersonData)

        }
        customElementManager.updateCustomElement(this.personDetailsElement, placeholders);

    }

    /**
     * Updates the details section when multiple persons are selected.
     * @param selection Array of selected GNDs
     */
    updateElementMultiple(selection: Array<string>) {

        let submissionListContainer = document.createElement("div");
        submissionListContainer.classList.add("goemap__submission_list_multiple")
        
        for (let GND of selection) {

            let selectedPersonData = personData["Personen"][GND];
            let heading = document.createElement("h4");
            heading.textContent = selectedPersonData["einsender"];
            submissionListContainer.appendChild(heading);
            submissionListContainer.appendChild(this.createSubmissionListElement(selectedPersonData));

        }

        let placeholders = {
            "num_selected": `${selection.length}`,
            "submission_list": submissionListContainer

        }
        customElementManager.updateCustomElement(this.personDetailsMultipleElement, placeholders);

    }



    /**
     * Creates a list of submissions
     * @param selectedPersonData Data of the selected person
     * @returns HTML element that contains information about the person's submissions
     */
    createSubmissionListElement(selectedPersonData): HTMLElement {

        let submissionListElement = document.createElement("div");
        submissionListElement.classList.add("goemap__submission_list");

        for (let submissionID of selectedPersonData["Einsendungen"]) {

            let submissionData = personData["Einsendungen"][submissionID];

            // Creates a button that makes the map pan to the place of submission
            let submissionPlaceButton;
            
            if (submissionData["Getty Koordinaten"]) {
                submissionPlaceButton = this.createGoToPlaceButton(submissionData["Einsendungsort"], [submissionData["Getty Koordinaten"]["latitude"], submissionData["Getty Koordinaten"]["longitude"]]);
            } else {
                submissionPlaceButton = "Keine Daten vorhanden";
            }

            let listItemElement = customElementManager.createCustomElement("goemap-submission-list-item",
                {
                    "title": submissionData["Titel"],
                    "author": submissionData["Autor"],
                    "submission_place": submissionPlaceButton,
                    "submission_date": submissionData["Einsendedatum"] ? submissionData["Einsendedatum"]: "Keine Daten vorhanden",
                }
            )

            submissionListElement.appendChild(listItemElement);

        }

        return submissionListElement;
    }

}