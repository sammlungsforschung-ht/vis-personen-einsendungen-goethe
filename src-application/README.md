# Readme Frontend

Das Frontend ist in TypeScript geschrieben und wird mit esbuild zu regulärem JavaScript kompiliert. Es werden keine UI frameworks genutzt, lediglich HTML templates, welche sich in `html/` befinden. Diese werden vom build script in die fertige `index.html` in `dist/` kopiert.


## Roadmap

- [x] Hauptlayout
- [x] Leaflet einbinden
- [x] Template für den Steckbrief
    - [x] Porträt
    - [x] Biographie lang
    - [x] Biographie kurz
    - [x] Links\*
    - [ ] Personenfunktion übersetzen und gendern
- [x] Template für Steckbrief bei Auswahl mehrerer Personen
- [x] Template für die Personeneinträge in der Liste
- [x] Einfach- und Mehrfachauswahl für die Personeneinträge
- [x] Koordinaten in Leaflet anzeigen
    - [x] Geburtsort
    - [x] Sterbeort
    - [x] Einsendungen
    - [x] Onclick und popups
    - [x] Symbole
    - [x] Marker für mehrere Objekte am selben Ort (ein Marker mit allen Daten in einem Popup)
- [ ] Stylesheet
    - [x] Mobil
    - [ ] Kontrastreich
    - [ ] Popups
    - [ ] Anpassungen (bei langen Texten, ausgefallenen Bildern, etc.)
- [ ] Accessibility
    - [x] Personenliste
    - [ ] Personendetails
    - [ ] Popups
- [ ] Laden der Daten
    - [x] Bezug der Daten aus personData.ts
    - [x] Links auf Steckbrief
    - [ ] Nicht geladene Bilder abfangen
    - [x] Orte mit gleichen Koordinaten großzügiger zusammenfassen
- [ ] Filterfunktion
    - [x] Filtern nach Funktion
    - [x] Sortieren nach Einsendungen
    - [ ] Sortieren nach Entfernung
    - [x] Filtern nach Themen

\* muss angepasst werden, wenn Datensatz verfügbar ist
 
## Struktur

`css/`: CSS source code.

`dist/`: Zielordner für den Build-Prozess

`html/`: Enthält HTML-Code der Anwendung, inklusive Templates

`ts/`: TypeScript-Code


## Abhängigkeiten

`leaflet`, `@types/leaflet` und `esbuild` werden als devDependencies benötigt (siehe `package.json`). Leaflet wird mittels esbuild in die JS-Datei gebündelt, daher ist es auch unter den Entwicklungsabhängigkeiten gelistet.

## Build

Die Daten müssen vor dem Build verarbeitet werden: `python3 process_data.py`. Es wird eine `data.json`-Datei im selben Ordner erwartet. Dabei werden die Daten als TypeScript-Modul in `ts/personData.ts` abgelegt. Die Daten werden dann in die fertige js-Datei integriert. Die resultierende Datei ist ca 2.7mb groß.

Danach entweder `npm run build` oder `python3 build.py`.