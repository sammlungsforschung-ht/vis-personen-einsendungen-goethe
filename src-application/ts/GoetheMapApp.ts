export { GoetheMapApp }
import { customElementManager, goeMapDebug } from './main'
import { MapViewer } from "./MapViewer"
import { PersonDetails } from './PersonDetails';
import { PersonList } from './PersonList';

class GoetheMapApp {

    private mainDiv: HTMLDivElement;       /** Main div of the application */
    private mapViewer: MapViewer;          /** Viewer for coordinates and places */
    private personDetailsViewer: PersonDetails;  /** Viewer for person details */
    private personList: PersonList;             /** List with all available persons */
    private personData: any;

    constructor(mainDiv: HTMLDivElement) {

        if (!mainDiv) {
            alert("Error: Main div could not be found.");
            return;
        }

        this.mainDiv = mainDiv;
        if (goeMapDebug) console.log("Goethe Map App constructed");

    }

    /**
     * Creates the HTML interface of the app
     */
    createInterface() {
        // Creates the multiselect sidebar on the left
        this.personList = new PersonList();

        // Creates the person information pane in the middle
        this.personDetailsViewer = new PersonDetails();
        
        // Creates the leaflet map
        this.mapViewer = new MapViewer();
        this.mapViewer.initLeafletMap();

        let footerElement = document.getElementById("goemap__footer");
        let footerContent = customElementManager.createCustomElement("goemap-footer", {"footer_text": `${new Date().toDateString()} (nicht alle Funktionen sind komplett implementiert, an Daten und Design wird noch gearbeitet)`});
        footerElement.appendChild(footerContent);

        let showPersonListButton = document.getElementById("goemap__show_person_list");

        showPersonListButton.onclick = () => {
            this.showPersonList();
        }

        // Initiates overview
        this.hidePersonDetails();
        this.personList.unselectAllElements();
        this.mapViewer.addOverviewMarkers(Object.keys(this.personList.listElements));
        this.showOverview();

        // Hides loading screen
        document.getElementById("goemap__loading").classList.add("goemap__loading--hidden");

    }

    showPersonDetails() {

        this.hideOverview();

        if (!this.mainDiv.classList.contains("goemap__wrapper--person_details_shown")) {
            this.mainDiv.classList.add("goemap__wrapper--person_details_shown");
            this.mapViewer.invalidateMapSize();
        }

    }

    hidePersonDetails() {

        this.mainDiv.classList.remove("goemap__wrapper--person_details_shown");
        this.mapViewer.invalidateMapSize();

    }

    showPersonList() {

        if (!this.mainDiv.classList.contains("goemap__wrapper--person_list_shown")) {
            this.mainDiv.classList.add("goemap__wrapper--person_list_shown");
            this.mapViewer.invalidateMapSize();
        }

    }

    hidePersonList() {

        this.mainDiv.classList.remove("goemap__wrapper--person_list_shown");
        this.mapViewer.invalidateMapSize();

    }



    /**
     * Updates the person details and the map according to the selected persons.
     * @param selection Array of person GNDs
     */
    updatePersonDetails(selection: Array<string>) {

        if (selection.length) {
            this.showPersonDetails();
            this.personDetailsViewer.updateData(selection);
            this.mapViewer.addMarkers(selection);
            this.hidePersonList();

        } else {
            this.showOverview();
        }

    }

    /**
     * Sets a selection in the person list
     * @param selection Array of person GNDs
     */
    setSelection(selection: Array<string>) {

        this.personList.setSelection(selection);

    }

    /**
     * Tells the map to focus a particular marker on the map
     * @param markerID ID of the marker
     */
    focusMarker(markerID:string) {

        this.mapViewer.focusMarker(markerID);

    }

    showOverview() {

        this.hidePersonDetails();
        this.mapViewer.clearCurrentMarkers();
        this.personList.unselectAllElements();
        this.mapViewer.showOverviewLayer();

        // setTimeout(() => { this.mapViewer.invalidateMapSize() }, 500);

    }

    updateOverview() {

        this.mapViewer.clearCurrentOverviewMarkers();
        this.mapViewer.addOverviewMarkers(Object.keys(this.personList.listElements));
        this.mapViewer.showOverviewLayer();

    }

    hideOverview() {

        this.mapViewer.hideOverviewLayer();

    }

}