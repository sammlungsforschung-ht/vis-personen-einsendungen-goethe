## Ziele


### Inhaltlich

- Die Personen hinter den Einsendungen von Goethes Privatbibliothek zugänglich machen
- Einfach zu bedienende Filterfunktionen um eine Untergruppe der Personen auszuwählen
- Liste der Personen mit Mehrfachauswahl
- Einzeige von Details zu den Ausgewählten Personen
- Einzeichnen der wichtigsten Orte der Person und Ort jeder Einsendung

### Technisch

- hohe Geschwindigkeit, kurze Latenz bei Auswahl von Personen
- keine unnötigen oder zu schweren Bibliotheken
- Kompatibilität mit mehr als 90% genutzer Browser
- selbst erstellte Elemente sollten möglichst barrierefrei sein
- nicht vorhandene Daten sollten abgefangen werden (Platzhalter für Portrait oder "Daten nicht vorhanden")
- soll auf einer statischen Seite laufen können


## Erwartungen

- Ladezeit ~1-2s beim Auswählen von allen Einträgen
- etwas Ladezeit beim Starten (alle Daten werden von Anfang an geladen)

## Bibliotheken

### Leaflet

- einfach
- OpenStreetMap kann eingebunden werden
- FOSS
- Marker, geometrische Formen, Pop-Ups und Klickevents

### TypeScript

- Übersichtlicher als JavaScript, weniger überraschungen


### esbuild

- schnellere Build-Zeiten im Vergleich zum TypeScript-Transpiler (Bruchteile einer Sekunde vs. Minuten)
- Erstellen einer einzelnen JS-Datei für das gesamte Projekt (inkl. Abhängigkeiten)021
- Kompatibilität von neueren Features für ältere Browser


## Technologien

### HTML-Templates

- in HTML eingebaut und einfach mit JavaScript zu verwalten
- HTML Templates werden benutzt um benutzerdefinierte HTML-Elemente zu erstellen
- Trennung von HTML Markup und JavaScript-Code
- Kompatibel mit 93% genutzer Browser
- sehr wenig Code, ersetzt GUI-Frameworks
- Benutzerdefiniertes Platzhalterelement `<fill-me>`




## Custom Elements



// As of August 2021, the shadow DOM makes styling efficiently impossible
// const shadowRoot = this.attachShadow({mode: 'open'})
//     .appendChild(template.cloneNode(true));

/*
    Ideally the template would be loaded right away, but...

    HTML elements are expected to be empty when they are being instantiated.
    The template needs to be loaded after instantiation of the HTML element. One way would be to use the
    connectedCallback, however:

    connectedCallback cannot be used here since the some of the code expects the 
    template to be loaded and parsed so that query selectors work. When connectedCallback runs, the 
    element might not have been completely parsed
    This is why the CustomElementManager class exists.
    loadTemplate() has to be called after element creation so that the element's
    HTML content is ready to work with.

    More on problems with connectedCallback:
    https://stackoverflow.com/questions/48498581/textcontent-empty-in-connectedcallback-of-a-custom-htmlelement
    https://stackoverflow.com/questions/48663678/how-to-have-a-connectedcallback-for-when-all-child-custom-elements-have-been-c

    Slot elements solve this problem, however, they introduce a lot of duplicate CSS code (due to the shadow DOM).
    This solution is a good tradeoff between stock HTML functionality, ease of use and maintainability.
    It's a very complex topic:
    https://www.smashingmagazine.com/2016/12/styling-web-components-using-a-shared-style-sheet/
*/