import { goeMapApp, goeMapDebug } from "./main";
import { customElementManager } from "./main";
import { personData } from "./personData";
export { PersonList };

/**
 * Accessible (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/listbox_role) listbox
 * that allows selection of multiple persons.
 * 
 * The term "person function" (DE: "Funktion der Person") refers to the function that the person has regarding the submitted book (printer, author, etc.)
 */
class PersonList {

    personListElement: HTMLUListElement;  /** Container div of the list */
    sortSelectElement: HTMLSelectElement;  /** Select element for selecting the sorthing method */
    /** Select element for selecting the person function to filter */
    filterFunctionSelectElement: HTMLSelectElement;  
    filterClassificationSelectElement: HTMLSelectElement;  
    selection: {[GND: string]: HTMLElement};  /** Currently selected persons (GND → list element) */
    listElements: {[GND: string]: HTMLElement};  /** ALl list elements (GND → list element) */
    personFunctions: {[GND: string]: Array<string>};  /** All "functions" of all persons */
    submissionClassifications: {[GND: string]: Array<string>};  /** Topics of all submissions per person */
    currentGNDs: Array<string>; /** List of currently filtered or sorted GNDs */

    constructor() {

        this.personListElement = document.getElementById("goemap__person_list") as HTMLUListElement;
        this.filterFunctionSelectElement = document.getElementById("goemap__filters_person_functions") as HTMLSelectElement;
        this.filterClassificationSelectElement = document.getElementById("goemap__filters_classifications") as HTMLSelectElement;
        this.sortSelectElement = document.getElementById("goemap__filters_sort") as HTMLSelectElement;
        this.personFunctions = {};
        this.submissionClassifications = {};
        this.selection = {};
        this.listElements = {};
        this.currentGNDs = [];
        this.createHTML();

        this.filterFunctionSelectElement.onchange = () => {
            this.applyFilters();
        }
        this.sortSelectElement.onchange = () => {
            this.applyFilters();
        }
        this.filterClassificationSelectElement.onchange = () => {
            this.applyFilters();
        }
        
    }

    /**
     * Handles clicks on list elements
     * @param e Event
     * @param gnd GND of the element clicked on
     * @param element HTML element that was clicked on
     */
    onClickElement(e, gnd: string, element: HTMLElement) {

        element.focus();

        // Click on single list element
        if (!(e.ctrlKey || e.shiftKey)) {
            this.unselectAllElements();
        }
        
        if (!(gnd in this.selection)) {
            // Selects element if it is not already selected
            this.selectElement(gnd);
        } else if (e.ctrlKey) {
            // Toggles selection of an element when CTRL is pressed
            this.unselectElement(gnd);
        }

        // Fills a row of selected list elements when SHIFT is pressed
        if (e.shiftKey) {
            this.fillSelection();
        }
        this.personListElement.setAttribute("aria-activedescendant", element.id);
        
        this.afterUpdateSelection();

    }

    afterUpdateSelection() {

        if (goeMapDebug) console.log("Selected GNDs:", Object.keys(this.selection));
        goeMapApp.updatePersonDetails(Object.keys(this.selection));

    }

    processOnKeyDown(event) {

        let element = event.originalTarget;
        let gnd = element.getAttribute("data-gnd");

        let updateSelection = false;

        // Space toggles selection
        if (event.code === "Space") {
            this.toggleSelection(gnd);
            updateSelection = true;

        // Up and down arrows focus previous and next list elements
        } else if (event.code === "ArrowDown" && element.nextElementSibling) {
            (element.nextElementSibling as HTMLElement).focus();
        } else if (event.code === "ArrowUp" && element.previousElementSibling) {
            (element.previousElementSibling as HTMLElement).focus();
        }

        if (updateSelection) {
            this.afterUpdateSelection();
        }

    }

    /**
     * Toggles selection of a specific list element by GND
     * @param gnd GND to toggle
     */
    toggleSelection(gnd: string) {
        if (gnd in this.selection) {
            this.unselectElement(gnd);
        } else {
            this.selectElement(gnd);
        }
    }

    /**
     * Removes selection from all list elements
     */
    unselectAllElements() {
        for (let gnd in this.selection) {
            this.selection[gnd].classList.remove("goemap__person_list_item--selected");
            this.selection[gnd].setAttribute("aria-selected", "false");
        }
        this.selection = {};
    }

    /**
     * Selects an element by provided GND
     * @param gnd GND to select
     */
    selectElement(gnd: string) {
        let element = this.listElements[gnd];

        this.selection[gnd] = element;
        if (!("goemap__person_list_item--selected" in element.classList)) {
            element.classList.add("goemap__person_list_item--selected");
            element.setAttribute("aria-selected", "true");
        }
    }

    /**
     * Applies a selection of GNDs
     * @param gnds GNDs to select
     */
    setSelection(gnds: Array<string>) {
        
        this.unselectAllElements();
        
        for (let gnd of gnds) {
            this.selectElement(gnd);
        }
        this.afterUpdateSelection();
        
    }
    
    /**
     * Deselects and element from the list
     * @param gnd GND to deselect
     */
    unselectElement(gnd: string) {
        let element = this.selection[gnd];
        element.classList.remove("goemap__person_list_item--selected");
        element.setAttribute("aria-selected", "false");
        delete this.selection[gnd];
    }

    /**
     * Selects all list elements between the first and last selected element
     */
    fillSelection() {
        let firstGND = Object.keys(this.selection)[0];
        let lastGND = Object.keys(this.selection)[Object.keys(this.selection).length -1];

        let startIndex = this.currentGNDs.indexOf(firstGND);
        let endIndex = this.currentGNDs.indexOf(lastGND);

        // Makes sure that the for loop works
        if (startIndex > endIndex) {
            let swap = startIndex;
            startIndex = endIndex;
            endIndex = swap;
        }
        
        for (let index = startIndex; index <= endIndex; index++) {
            this.selectElement(this.currentGNDs[index]);
        }

    }

    /**
     * Handles focusing a list element for accessibility
     * @param event Event from focusing
     */
    onFocusListElement(event) {

        // if (event.originalTarget && event.relatedTarget && event.originalTarget.parentNode != event.relatedTarget.parentNode) {
            
        //     if (Object.keys(this.selection).length) {
        //         this.selection[Object.keys(this.selection)[0]].focus();
        //     } else {
        //         this.listElements[Object.keys(this.listElements)[0]].focus();
        //     }

        // }

    }

    /**
     * Fills the list of persons
     * @param personGNDs List of GNDs to display
     * @param initial Whether this function is being run for the first time
     */
    fillList(personGNDs: Array<string>, initial: boolean=false) {
        
        for (let gnd of personGNDs) {
            

            let person = personData["Personen"][gnd];

            if (!person) {
                continue;
            }

            /* When the list is being filled for the first time, information for the filters is being gathered.
               This way the person list can be filtered without any loading times.
            */
            if (initial) {
                
                for (let submissionID of person["Einsendungen"]) {
                    // Gathers all topics and functions
                    let submissionTopic = personData["Einsendungen"][submissionID]["Prim\u00e4rklassifikation"];
                    if (!this.submissionClassifications[submissionTopic]) {
                        this.submissionClassifications[submissionTopic] = [];
                    }
                    this.submissionClassifications[submissionTopic].push(gnd)
                    
                    // Gathers all person "functions"/occupations
                    let personFunction = personData["Einsendungen"][submissionID]["Einsender Funktion"];
                    if (!this.personFunctions[personFunction]) {
                        this.personFunctions[personFunction] = [];
                    }
                    if (!this.personFunctions[personFunction].includes(gnd)) {
                        this.personFunctions[personFunction].push(gnd);
                    }

                }

            }
            let slots = {
                "person_name": person["einsender"] as string,
                "submission_count": `${person["Einsendungen"].length}`,
                "string_submission_count": "Einsendungen"
            }
            let listElement = customElementManager.createCustomElement("goemap-person-list-item", slots);
            
            // Accessibility
            listElement.setAttribute("role", "option");
            listElement.setAttribute("aria-selected", "false");
            listElement.tabIndex = 0;
            
            listElement.classList.add("goemap__person_list_item")
            listElement.id = `goemap__person_list_item_${gnd}`;
            listElement.setAttribute("data-gnd", gnd);
            listElement.onclick = (event) => { this.onClickElement(event, gnd, listElement); }
            listElement.onfocus = (event) => { this.onFocusListElement(event); }

            this.listElements[gnd] = listElement;
            this.personListElement.appendChild(listElement);

        }
    }

    /**
     * Creates HTML content of the list
     */
    createHTML() {
        
        this.personListElement.onkeydown = (event) => {
            this.processOnKeyDown(event);
        }

        this.fillList(this.getFilteredGNDs(), true);

        for (let personFunction in this.personFunctions) {
            let newOption = document.createElement("option");
            newOption.textContent = personFunction;
            this.filterFunctionSelectElement.appendChild(newOption);
        }

        for (let classification in this.submissionClassifications) {
            let newOption = document.createElement("option");
            newOption.textContent = classification;
            this.filterClassificationSelectElement.appendChild(newOption);
        }

    }

    /**
     * Sorts GNDs by the number of book submissions of the person they belong too
     * @param sortArray Array of keys/GNDs
     * @param descending Whether to sort the list in descending order
     * @returns List of GNDs sorted by the distance to Weimar
     */
    sortByNumSubmissions(sortArray: Array<string>, descending: boolean=false) {
        var items = sortArray.map(function(gnd) {
            if (personData["Personen"][gnd]) {
                return [gnd, personData["Personen"][gnd]["Einsendungen"].length];
            } else {
                return [gnd, 0];
            }
        });
        
        items.sort(function(first, second) {

            if (descending) {
                return second[1] - first[1];
            } else {
                return first[1] - second[1];
            }

        });

        // Returns sorted array of GNDs
        return items.map( (item) => {
            return item[0];
        } )

    }

    /**
     * Sorts GNDs by the distance of the birth place to Weimar
     * @param sortArray Array of keys/GNDs
     * @param descending Whether to sort the list in descending order
     * @returns List of GNDs sorted by the number of submissions
     */
     sortByDistanceToWeimar(sortArray: Array<string>, descending: boolean=false) {

         var items = sortArray.map(function(gnd) {
             if (personData["Personen"][gnd] && personData["Personen"][gnd]["distanceWeimar"] != 999) {  
                return [gnd, personData["Personen"][gnd]["distanceWeimar"]];
            } else {
                if (descending) {
                    return [gnd, 999];
                } else {
                    return [gnd, 0];
                }
            }
        });
        
        items.sort(function(first, second) {

            if (descending) {
                return first[1] - second[1];
            } else {
                return second[1] - first[1];
            }

        });

        // Returns sorted array of GNDs
        return items.map( (item) => {
            return item[0];
        } )

    }

    /**
     * Filters the GNDs according to user input
     * @returns Filtered GNDs
     */
    getFilteredGNDs(): Array<string> {
           
        let personFunctionValue = this.filterFunctionSelectElement.value;
        let personClassificationValue = this.filterClassificationSelectElement.value;
        let sortByValue = this.sortSelectElement.value;
        
        let filteredGNDs = [];
    
        if (personFunctionValue === "all") {
            filteredGNDs = Object.keys(personData["Personen"]);
        } else if (personFunctionValue in this.personFunctions) {
            filteredGNDs = this.personFunctions[personFunctionValue];
        }

        if (personClassificationValue != "all") {
            filteredGNDs = filteredGNDs.filter( (element, index, array) => {
                return this.submissionClassifications[personClassificationValue].includes(element);
            });
        }
    
        if (sortByValue === "submissions_asc") {
            filteredGNDs = this.sortByNumSubmissions(filteredGNDs, false);
        } else if (sortByValue === "submissions_desc") {
            filteredGNDs = this.sortByNumSubmissions(filteredGNDs, true);
        } else if (sortByValue === "distance_weimar_asc" ) {
            filteredGNDs = this.sortByDistanceToWeimar(filteredGNDs, true)
        } else if (sortByValue === "distance_weimar_desc" ) {
            filteredGNDs = this.sortByDistanceToWeimar(filteredGNDs, false)
        }

        this.currentGNDs = filteredGNDs;

        return filteredGNDs;

    }

    /** Applies selected filters */
    applyFilters() {

        // Clears everything
        this.personListElement.innerHTML = "";
        this.listElements = {};
        this.unselectAllElements();

        let filteredGNDs = this.getFilteredGNDs();

        this.fillList(filteredGNDs);

        goeMapApp.updateOverview();

    }

}