import json
from math import sqrt

json_data_working = None


def get_distance(x1, x2):
    return sqrt((x2[0] - x1[0])**2 + (x2[1] - x1[1])**2)


# Rounds coords so that all occurring coordinates pointing to one place have the same values
def fix_data(data):

    for person_key in data["Personen"]:

        person = data["Personen"][person_key]

        if person and "placeOfBirth" in person and person["placeOfBirth"]:
            person["placeOfBirth"]["latitude"] = round(float(person["placeOfBirth"]["latitude"]), 2)
            person["placeOfBirth"]["longitude"] = round(float(person["placeOfBirth"]["longitude"]), 2)
            w1 = 50.983333
            w2 = 11.316667

            o1 = person["placeOfBirth"]["latitude"]
            o2 = person["placeOfBirth"]["longitude"]

            person["distanceWeimar"] = get_distance((w1, w2), (o1, o2))
        elif person:
            person["distanceWeimar"] = 999



        if person and "placeOfDeath" in person and person["placeOfDeath"]:
            person["placeOfDeath"]["latitude"] = round(float(person["placeOfDeath"]["latitude"]), 2)
            person["placeOfDeath"]["longitude"] = round(float(person["placeOfDeath"]["longitude"]), 2)


    for submission_key in data["Einsendungen"]:

        submission = data["Einsendungen"][submission_key]

        del submission["Getty Koordinaten"]  # deletes broken entry (random number)
        # uses value from older working file
        submission["Getty Koordinaten"] = json_data_working["Einsendungen"][submission_key]["Getty Koordinaten"]

        if submission and "Getty Koordinaten" in submission and submission["Getty Koordinaten"]:
            submission["Getty Koordinaten"]["latitude"] = round(float(submission["Getty Koordinaten"]["latitude"]), 2) 
            submission["Getty Koordinaten"]["longitude"] = round(float(submission["Getty Koordinaten"]["longitude"]), 2) 


# Fixes keys with double quotes
def fix_keys(data):

    for person_key in data["Personen"]:
        person = data["Personen"][person_key]

        if person:
            person["Einsendungen"] = person["\"Einsendungen\""]
            del person["\"Einsendungen\""]

    for submission_key in data["Einsendungen"]:
        submission = data["Einsendungen"][submission_key]
        
        if submission and '"Einsender Funktion"' in submission:
            submission["EinsenderFunktion"] = submission['"Einsender Funktion"']
            del submission['"Einsender Funktion"']


def main():

    json_data = None

    with open("data.json") as file:
        json_data = json.load(file)
        fix_keys(json_data)
        fix_data(json_data)

    print(json_data)
 
    person_data_file = "export {personData};let personData="

    with open("ts/personData.ts", "w") as file:
        
        file.write(person_data_file + json.dumps(json_data))

# Temporary: loads older working version (never version is missing submission coordinates)
with open("data_working.json") as file:
    json_data_working = json.load(file)
    fix_keys(json_data_working)

main()