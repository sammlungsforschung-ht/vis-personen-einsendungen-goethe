import requests
import pandas as pd
import time
from io_tools import save_data_to_file

pd.set_option('display.max_columns', None)


class DataEnrichment:

    def __init__(self):
        self.nr_of_submissions_without_gnd = 0
        self.outdated_gnds = 0  # GNDs in dataset, that are not anymore used as GND

    # data from lobid has to be parsed for correct place of deutsche-biographie-entry
    def parse_for_deutsche_biographie(self, response: dict) -> str:

        i = 0
        for entry in response['sameAs'][0:20]:  # gehardcoded. Unklar, was maximale Anzahl an Eintraegen ist
            if i == len(response['sameAs']):
                return
            if "biographie" in entry['id']:
                print(response['sameAs'][i]['id'])
                return response['sameAs'][i]['id']
            i += 1

    def parse_for_wikidata_entry(self, response: dict) -> str:

        for entry in response['sameAs']:
            print(entry)
            if 'abbr' in entry['collection'] and 'WIKIDATA' in entry['collection']['abbr']:
                print("Wikidata-Id is: " + entry['id'])
                return entry['id']

    # @param input:http://www.wikidata.org/entity/QID
    # @param output: https://www.wikidata.org/wiki/Special:EntityData/QID.json
    # automatic resolution of urls does not seem to work in every case

    def get_special_entity_wikidata(self, wikidata_link: str) -> str:
        return wikidata_link[wikidata_link.rfind('/') + 1:len(wikidata_link)]

    def resolve_place(self, session: requests.Session(), q_id: str) -> dict:
        entry_from_wikidata = session.get(
            f'https://www.wikidata.org/w/api.php?action=wbgetentities&ids={q_id}&props=labels|claims&format=json')

        local_entry = {}
        print("Q-ID for geographical Entity: " + q_id)

        if 'P625' not in entry_from_wikidata.json()['entities'][q_id]['claims']:
            return None

        local_entry['latitude'] = \
        entry_from_wikidata.json()['entities'][q_id]['claims']['P625'][0]['mainsnak']['datavalue']['value']['latitude']
        local_entry['longitude'] = \
        entry_from_wikidata.json()['entities'][q_id]['claims']['P625'][0]['mainsnak']['datavalue']['value']['longitude']

        if 'de' in entry_from_wikidata.json()['entities'][q_id]['labels']:
            local_entry['name'] = entry_from_wikidata.json()['entities'][q_id]['labels']['de']['value']

        # in case there is no german name for the place use english name
        else:
            if 'en' in entry_from_wikidata.json()['entities'][q_id]['labels']:
                local_entry['name'] = entry_from_wikidata.json()['entities'][q_id]['labels']['en']['value']
        return local_entry

    # create right date format as year-month-day (in decimals)
    def parse_date(self, raw_data: str) -> str:
        return raw_data[1:10]

    # input: wikipedia-link from lobid
    # ouput: link for downloading json-file with necessary parameters
    def get_url_for_wiki_api(self, link: str) -> str:
        name_identifier = link[link.rfind('/') + 1:len(link)]
        return f'https://de.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&titles={name_identifier}&exintro=&exsentences=10&explaintext=&redirects=&formatversion=2'

    def request_lobid_entry(self, session: requests.Session(), gnd: int, item: dict) -> dict:
        print(type(item))
        print(item.keys())
        print(item)
        # in case there is no GND in the file
        # abort the retrieval for this entry
        if gnd.strip() == 'no GND':
            print("No GND to search for..")
            self.nr_of_submissions_without_gnd += 1
            return

        return_data = session.get(f'https://lobid.org/gnd/{gnd}.json')

        # in case this gnd does not exist anymore, abort retrieval for this entry
        if return_data.text[0:9] == 'Not found':
            self.outdated_gnds += 1
            return

        return_dict = return_data.json()
        entry_dict = {}

        if 'depiction' in return_dict:
            entry_dict["bild_link"] = return_dict['depiction'][0]['id']

        if 'wikipedia' in return_dict.keys():
            entry_dict["wikipedia_link"] = return_dict['wikipedia'][0]['id']

        entry_dict["deutsche_biographie_link"] = self.parse_for_deutsche_biographie(return_dict)
        entry_dict['wikidata_link'] = self.parse_for_wikidata_entry(return_dict)
        entry_dict['einsender'] = item['Einsender']
        entry_dict['Einsender Funktion'] = item['Einsender Funktion']
        entry_dict['"Einsendungen"'] = item['Einsendungen']

        return entry_dict

    # session: Session-Object to handle the requests without establishing each time a new connection
    # item: person-entry with gnd as key and other data as values
    def request_wikidata_entry(self, session: requests.Session(), key: str, value: dict) -> dict:
        gnd_key = key
        entry_dict = {gnd_key: value}

        if value["wikidata_link"] is not None:
            entity_link = self.get_special_entity_wikidata(value["wikidata_link"])
            wikidata_return = session.get(
                f'https://www.wikidata.org/wiki/Special:EntityData/{entity_link}.json')  # use this method to extract url for api-access
            wikidata_entry = wikidata_return.json()
            print("Q-ID of entry: " + str(entity_link))
        else:
            return {'placeOfBirth': None,
                    'placeOfDeath': None}

        if 'P19' in wikidata_entry['entities'][entity_link]['claims']:
            entry_dict[gnd_key]['placeOfBirth'] = self.resolve_place(session,
                                                                     wikidata_entry['entities'][entity_link]['claims'][
                                                                         'P19'][0]['mainsnak']['datavalue']['value'][
                                                                         'id'])
        else:
            entry_dict[gnd_key]['placeOfBirth'] = None

        # sometimes there is no place of death listed
        if 'P20' in wikidata_entry['entities'][entity_link]['claims']:
            entry_dict[gnd_key]['placeOfDeath'] = self.resolve_place(session,
                                                                     wikidata_entry['entities'][entity_link]['claims'][
                                                                         'P20'][0]['mainsnak']['datavalue']['value'][
                                                                         'id'])
        else:
            entry_dict[gnd_key]['placeOfBirth'] = None

        if 'P569' in wikidata_entry['entities'][entity_link]['claims']:
            if 'datavalue' in wikidata_entry['entities'][entity_link]['claims']['P569'][0]['mainsnak']:
                entry_dict[gnd_key]['dateOfBirth'] = self.parse_date(
                    wikidata_entry['entities'][entity_link]['claims']['P569'][0]['mainsnak']['datavalue']['value'][
                        'time'])
        else:
            entry_dict[gnd_key]['dateOfBirth'] = None

        if 'P570' in wikidata_entry['entities'][entity_link]['claims']:
            if 'datavalue' in wikidata_entry['entities'][entity_link]['claims']['P570'][0]['mainsnak']:
                entry_dict[gnd_key]['dateOfDeath'] = self.parse_date(
                    wikidata_entry['entities'][entity_link]['claims']['P570'][0]['mainsnak']['datavalue']['value'][
                        'time'])
        else:
            entry_dict[gnd_key]['dateOfDeath'] = None

        return entry_dict

    def get_abstract_from_wikipedia(self, session: requests.Session, link: str) -> str:
        output = session.get(link)
        output_dict = output.json()
        text = output_dict['query']['pages'][0]['extract']  # Achtung: Abstract ist ein langer String
        return text

    # Method to extract name, longitude, latitude from Getty-Id of submission place
    def get_getty_data(self, session: requests.Session(), getty_number: int) -> dict:
        getty_entry = {'latitude': None, 'longitude': None}

        if getty_number == 0:
            return None
        elif getty_number == 7003460:  # file for 7003460 is too big to be processed, needs to be hardcoded, because there is only one entry with this getty-id
            getty_entry['latitude'] = 50.5000
            getty_entry['longitude'] = 5.5000
            return getty_entry

        if session.get(f'http://vocab.getty.edu/tgn/{getty_number}.json').status_code != 200:
            print(
                "Error, Statuscode: " + str(session.get(f'http://vocab.getty.edu/tgn/{getty_number}.json').status_code))
            return None

        dictionary = session.get(f'http://vocab.getty.edu/tgn/{getty_number}.json').json()

        for entry in dictionary['results']['bindings']:
            if 'latitude' in entry['Predicate']['value']:
                getty_entry['latitude'] = entry['Object']['value']
            if 'longitude' in entry['Predicate']['value']:
                getty_entry['longitude'] = entry['Object']['value']
        return getty_entry

    # method to load list of gnds and execute each request request for lobid synchronously
    def lobid_wrapper_function(self, person_dictionary: dict):
        sess = requests.Session()
        log_file = open('log_lobid.txt', 'w')
        person_dict_copy = {}

        start = time.time()
        for gnd, dict_entry in person_dictionary.items():
            person_dict_copy[gnd] = self.request_lobid_entry(sess, gnd, dict_entry)

        duration = time.time() - start
        print("Number of GNDs, that are not used anymore: " + str(self.outdated_gnds))
        print("Number of Submissions without GND: " + str(self.nr_of_submissions_without_gnd))
        print("Elapsed time: " + str(duration))

        log_file.write("Number of GNDs, that are not used anymore: " + str(self.outdated_gnds))
        log_file.write("Number of Submissions without GND: " + str(self.nr_of_submissions_without_gnd))
        save_data_to_file(person_dict_copy, '../data/lobid_output_final.json')
        return person_dict_copy

    def wikidata_wrapper(self, person_dictionary: dict):
        copy_with_places = {key: None for key in person_dictionary.keys()}
        session = requests.session()
        start = time.time()
        i = 0
        for gnd, entry in person_dictionary.items():
            print("number of current Wikidata_entry: " + str(i))
            if gnd == 'no GND' or entry is None:
                print("No GND for wikidata")
                continue
            copy_with_places.update(self.request_wikidata_entry(session, gnd,
                                                                entry))  # zurückgegebenes dictionary mit key in neues dictionary einhängen
            i += 1
        print("Elapsed time is: " + str(time.time() - start))
        save_data_to_file(copy_with_places, '../data/wikidata_output_final.json')

    def wikipedia_abstract_wrapper(self, person_dictionary: dict) -> dict:
        session = requests.session()
        print(person_dictionary.values())

        for gnd, entry in person_dictionary.items():
            if entry is not None and 'wikipedia_link' in entry:
                entry['wikipedia zusammenfassung'] = self.get_abstract_from_wikipedia(session,
                    self.get_url_for_wiki_api(entry['wikipedia_link']))
            else:
                continue

        save_data_to_file(person_dictionary, 'wikipedia_out.json')
        return person_dictionary

    def getty_thesaurus_wrapper(self, submissions_dictionary: dict) ->dict:
        getty_lookup_dict = {}
        session = requests.session()
        start = time.time()
        for submission_id, submission in submissions_dictionary.items():
            for entries in submission:

                if entries == 'Getty Koordinaten' and submission[
                    'Getty Koordinaten'] not in getty_lookup_dict.keys():  # avoid requesting entries, that have been already retrieved
                    temp = submission['Getty Koordinaten']
                    submission['Getty Koordinaten'] = self.get_getty_data(session, submission['Getty Koordinaten'])
                    getty_lookup_dict[temp] = submission['Getty Koordinaten']
                    print("Key does not exist yet")
                elif entries == 'Getty Koordinaten' and submission['Getty Koordinaten'] in getty_lookup_dict.keys():
                    submission['Getty Koordinaten'] = getty_lookup_dict[submission['Getty Koordinaten']]
                    print("Key already there")

        print("Elapsed time: " + str(time.time() - start))
        save_data_to_file(submissions_dictionary, '../data/getty_thesaurus_output_final.json')
        return submissions_dictionary

    if __name__ == '__main__':
        pass
        # lobid_wrapper_function
