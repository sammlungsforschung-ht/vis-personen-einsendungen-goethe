export { CustomElementManager };

/**
 * Custom HTML element similar to <slot> only that it does not use the shadow DOM.
 * This element can be used in HTML templates so that ths CustomElementManager can replace them 
 * with other elements.
 */
class FillMe extends HTMLElement {
    constructor() {
        super();
    }
}
  

/**
 * Defines custom HTML elements from templates to keep markup creation in script code to a minimum.
 */
class CustomElementManager {

    /* These templates can be found in the html source folder (*.template.html)) */
    static templates: { [index: string]: any; } = {
        "goemap-person-list-item": "goemap_tmp__person_list_item",
        "goemap-person-details": "goemap_tmp__person_details",
        "goemap-person-details-multiple": "goemap_tmp__person_details_multiple",
        "goemap-footer": "goemap_tmp__footer",
        "goemap-submission-popup": "goemap_tmp__submission_popup",
        "goemap-birth-popup": "goemap_tmp__birth_popup",
        "goemap-death-popup": "goemap_tmp__death_popup",
        "goemap-overview-popup": "goemap_tmp__overview_popup",
        "goemap-submission-list-item": "goemap_tmp__submission_list_item"
    }

    constructor() {
    
        customElements.define('fill-me', FillMe);
    
    }

    /**
     * Defines all custom elements defined by HTML templates
     */
    defineAllCustomElements() {

        for (let elementName in CustomElementManager.templates) {
            this.defineCustomElement(elementName, CustomElementManager.templates[elementName]);
        }

    }

    /**
     * Defines a custom HTML element
     * https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_templates_and_slots
     * @param elementName Desired name of the element
     * @param templateID HTML ID of the template the element will be based on
     */
    defineCustomElement(elementName: string, templateID: string) {

        customElements.define(elementName,
            class extends HTMLElement {


                constructor() {
                    super();
                    
                    /**
                     * The template has to be loaded at a later point.
                     * See project documentation on custom elements
                     */
                    
                }

                loadTemplate() {
                    const templateContent = (document
                        .getElementById(templateID) as HTMLTemplateElement)
                        .content;
                    const temp = document.importNode(templateContent, true);
                    this.appendChild(temp);
                }

            }
        );

    }

    /**
     * Fills a placeholder element
     * @param placeholderElement Placeholder element to be filled/replaced
     * @param slots Slots of the placeholder element to be filled. This can be either text content or ready HTML elements
     */
    fillPlaceholder(placeholderElement: HTMLElement, slots: {[slotName: string]: string | HTMLElement}) {

        let placeholderName = placeholderElement.getAttribute("name");
    
        if (placeholderName in slots) {
    
            let filler: HTMLElement;
    
            if (typeof slots[placeholderName] === "string") {  // The slot is text so it creates a span element to contain it
                filler = document.createElement("span");
                (filler as HTMLSpanElement).textContent = slots[placeholderName] as string;
            } else if (slots[placeholderName] instanceof HTMLElement) {                                           // The slot is an HTML Element, there is nothing to do
                filler = slots[placeholderName] as HTMLElement;
            } else {
                filler = document.createElement("span");
                (filler as HTMLSpanElement).textContent = "?";
            }
    
            filler.setAttribute("was-placeholder", "true");
            filler.setAttribute("name", placeholderName);
    
            placeholderElement.parentNode.replaceChild(filler, placeholderElement);
        }

    }

    /**
     * Creates a custom element and fills its slots
     * @param elementName Element name of the custom element
     * @param slots {slotName → HTML} "parameters" for the custom element
     */
    createCustomElement(elementName: string, slots: {[slotName: string]: string | HTMLElement}): HTMLElement {

        let newElement = document.createElement(elementName);
        // The custom element class is defined dynamically in the function defineCustomElement
        //@ts-ignore
        newElement.loadTemplate();

        let placeholderElements = newElement.querySelectorAll("fill-me");

        for (let placeholderElement of placeholderElements) {
            this.fillPlaceholder((placeholderElement) as HTMLElement, slots);
        }

        return newElement;

    }

    /**
     * Updates a placeholder element that has already been filled with content before.
     * Placeholder slot names have been kept in the element and can still be used like when the element was created.
     * @param element Element to be updated
     * @param slots Slots of the element to update
     */
    updateCustomElement(element: HTMLElement, slots: {[id: string]: string | HTMLElement}) {

        let placeholders = Array.from(element.querySelectorAll('[was-placeholder="true"]'));
        placeholders = placeholders.concat(Array.from(element.querySelectorAll("fill-me")));

        for (let placeholderElement of placeholders) {
            this.fillPlaceholder((placeholderElement as HTMLElement), slots);
        }

    }

}