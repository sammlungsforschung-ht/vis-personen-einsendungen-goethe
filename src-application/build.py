import os
import shutil
import re


def prepare_html_code():

    templates_html = ""
    app_html = ""

    # Gathers all templates and merges them into a single string
    for fname in os.listdir("html"):
        if "template" in  fname:
            with open(os.path.join("html", fname), "r") as f:
                # Reads template and strips HTML comments
                templates_html += re.sub("(<!--.*?-->)", "", f.read(), flags=re.DOTALL)
                templates_html += "\n"
    
    # Reads the main html file
    with open(os.path.join("html", "goe_map_app.partial.html"), "r") as f:
        app_html = f.read()

    # Returns the entire code that is necessary for the app to function
    return templates_html + app_html


def transpile():

    main_ts_path = os.path.join("ts", "main.ts")
    outfile_path = os.path.join("dist", "GoetheMapApp.js")
    os.system(f"esbuild {main_ts_path} --bundle --outfile={outfile_path}")


def copy_files():

    shutil.copy(os.path.join("node_modules", "leaflet", "dist", "leaflet.js"), "dist")
    shutil.copy(os.path.join("node_modules", "leaflet", "dist", "leaflet.css"), "dist")
    shutil.copytree(os.path.join("node_modules", "leaflet", "dist", "images"), os.path.join("dist", "images"))
    shutil.copy(os.path.join("css", "goemap_style.css"), "dist")

    for image in ["marker-icon-asterisk.png", "marker-icon-dagger.png", "marker-icon-ellipsis.png", "placeholder_portrait_de.png"]:
        shutil.copy(os.path.join("img", f"{image}"), os.path.join("dist", "images"))



def write_html():

    app_html = prepare_html_code()

    with open(os.path.join("html", "index.html"), "r") as f:
        html = f.read();
        html = html.replace("<!--{{ goe_map_code }}-->", app_html)

        with open(os.path.join("dist", "index.html"), "w") as outf:
            outf.write(html)


def main():
    
    if os.path.isdir("dist"):
        shutil.rmtree("dist")
        os.mkdir("dist")

    transpile()
    copy_files()
    write_html()


if __name__ == "__main__":
    main()