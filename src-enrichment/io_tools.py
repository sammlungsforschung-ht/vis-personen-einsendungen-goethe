from collections import Counter
import pandas as pd
import simplejson

pd.set_option('display.max_columns', None)


def load_data(filepath:str) ->pd.DataFrame:
    submissions_data = pd.read_excel(filepath)
    return submissions_data


def export_to_json(dataframe: pd.DataFrame, filepath: str):
    dataframe.to_json(filepath, lines=True, orient="records")


# input: DataFrame generated from excel-file
# output: Dictionary of submissions with title, author, place...
def transform_submissions(filepath: str, submissions: pd.DataFrame) -> dict:
    submissions = submissions.to_dict(orient='records')  # creates list of dictionaries
    submissions_dict = {}

    for line in submissions:
        submissions_dict[line['ID']] = {"Titel": line['Titel'],
                                        "Autor": line['Autor/in'],
                                        "Ort": line['Ort'],
                                        "Einsendungsort": line['Einsendung/ Übergabe Ort'],
                                        "Getty Koordinaten": line['Ort im Getty Thesaurus'],
                                        "Primärklassifikation": line['Primärklassifikation'],
                                        "Sekundärklassifikation": line['Weitere Klassifikationen'],
                                        "Einsendedatum": line['Datum (Brief oder Dokumentation)'],
                                        "Quelle Einsendung": line['Quelle für Einsendung'],
                                        "Digilink": line['DigiLink'],
                                        "Digi-Quelle": line['DigiQuelle'],
                                        "Einsender Funktion": line['Einsender Funktion']
                                        }
    save_data_to_file(submissions_dict, filepath)
    return submissions_dict


def load_excel_data(filepath: str) -> pd.DataFrame:
    submissions_data = pd.read_excel(filepath)
    return submissions_data


def load_internal_data(filepath: str) -> dict:
    with open(filepath, 'r') as file_handle:
        data = simplejson.load(file_handle)
    return data


def save_data_to_file(dictionary: dict, outfile: str):
    with open(outfile, 'w') as file:
        output = simplejson.dumps(dictionary)
        file.write(output)
    print("file has been written")

def transform_data(filepath:str, submissions: pd.DataFrame):
    print("---------")
    print(type(filepath))
    print("---------")
    file = open(filepath,'a')
    person_dict = submissions.to_dict(orient='records')
    pers = {}
    nr_no_gnd_entries = 0
    for item in person_dict:
        if item['Einsender GND'] == 'no GND':
            nr_no_gnd_entries += 1
        pers[item['Einsender GND']] = {"Einsender": item["Einsender / Überbringer"]}
    file.write("Nr of entries with no GND" + str(nr_no_gnd_entries))
    file.close()

    return person_dict

def remove_columns(df:pd.DataFrame, columns_to_remove: list) ->pd.DataFrame:
    print(df.columns)
    df = df.drop([0])
    df = df.drop(columns_to_remove, axis=1)

    return df


#print and save statistics from the submissions-table
def log_statistics(filepath:str, dataframe:pd.DataFrame):

    file = open(filepath, 'a')

    nr_unique_gnds = str(dataframe["Einsender GND"].nunique())
    nr_unique_persons = str(dataframe["Einsender GND"].nunique())
    nr_entries = str(len(dataframe["Laufende Nr."]))
    nr_unique_authors = str(dataframe["Autor/in"].nunique())

    nr_anonymous_pubs = str(sum(dataframe['Autor/in'] == '[Anonymous publication]'))
    #nr_anonymous_pubs2 = str(len(dataframe[dataframe['Autor/in'] == '[Anonymous publication]']))


    print(Counter(dataframe["Einsender Funktion"]))

    print("Number of anonymous Publications: " + nr_anonymous_pubs)
    print("Number of unique Senders GNDs: " + nr_unique_gnds)
    print("Number of unique Senders: " + nr_unique_persons)
    print("Number of entries: " + nr_entries)
    print("Number of unique authors of submitted texts: " + nr_unique_authors)
    file.write("Number of anonymous Publications: " + nr_anonymous_pubs + '\n')
    file.write("Number of unique Senders-GNDs: " + nr_unique_gnds + '\n')
    file.write("Number of unique Senders: " + nr_unique_persons + '\n')
    file.write("Number of entries: " + nr_entries + '\n')
    file.write("Frequency Distribution of Submitter Categories: " + str(Counter(dataframe["Einsender Funktion"])))
    file.write("Number of unique authors of submitted texts: " + nr_unique_authors)
    #file.write(nr_anonymous_pubs)
    file.close()
#wrapper-function to include all the necessary processes to load and construct the initial data
def load_and_preprocess_data(in_path, out_path) -> dict:
    df = load_excel_data(in_path)
    columns_to_remove = ['Position im Regal', 'Ruppert', 'Einsender / Überbringer2', 'Bd./Copy', 'OPAC', 'RegestLink',
     'RuppertSeite', 'RuppertLink', 'Unnamed: 28', 'Konkordanz Regestausgabe', 'Spuren im Buch']
    df = remove_columns(df, columns_to_remove)
    #export_to_json(df,out_path)
    #file = open('./statistics_log.txt', 'w')
    #log_statistics('./statistics_log.txt', df)
    #person_dict = transform_data('./statistics_log.txt', df)

    #person_dict = transform_data('./statistics_log_2.txt', df)
    person_dict = None
    submissions_dict = transform_submissions('../data/submissions.json', df)
    #save_data_to_file(person_dict, out_path)
    return person_dict, submissions_dict


