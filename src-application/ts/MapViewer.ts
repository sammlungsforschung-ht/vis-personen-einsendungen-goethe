import L, { Layer, Map, marker, Marker, Popup, popup, tooltip } from "leaflet";
import { customElementManager, goeMapDebug } from "./main";
import { personData } from "./personData";
import { goeMapApp } from "./main";
import { MarkerData } from "./MarkerData";

export { MapViewer };

class MapViewer {

    private mapDivID: string;
    private leafletMap: Map;
    private currentMarkers: { [id: string]: Marker };
    private currentOverviewMarkers: { [id: string]: Marker };
    private markerData: any;
    private overviewLayer: Layer;
    private overviewMarkerData: any;

    constructor() {

        this.mapDivID = "goemap__map";

        this.currentMarkers = {};
        this.currentOverviewMarkers = {};
        this.markerData = {};
        this.overviewMarkerData = {};

    }

    /**
     * Creates a leaflet map and loads an OpenStreetMap layer.
     */
    initLeafletMap() {

        this.leafletMap = L.map(this.mapDivID, {
            center: [50.9794934, 11.3235439],
            zoom: 13,
        });
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            'attribution':  'Kartendaten &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>-Mitwirkende',
            }).addTo(this.leafletMap);

        // Defines actions that makes sure that portraits in popups are loaded when the popup is first opened
        this.leafletMap.on("popupopen", function(e: unknown) {

            let popupContent = (e as {popup: Popup}).popup.getContent();
                for (let element of Array.from(popupContent.querySelectorAll("[data-imgsrc]")) as Array<HTMLImageElement>) {
                    
                    // Swaps the data-imgsrc attribute for the actual src attribute which then makes the portrait load
                    if (element.getAttribute("data-imgsrc")) {
                        element.src = element.getAttribute("data-imgsrc");
                    }
                }
        })
    }

    /**
     * Clears all markers that are currently displayed on the map.
     */
    clearCurrentMarkers() {

        for (let marker of Object.values(this.currentMarkers)) {
            marker.remove();
        }

        this.currentMarkers = {};
        this.markerData = {};

    }


    /**
     * Clears all markers that are currently displayed on the map.
     */
    clearCurrentOverviewMarkers() {

        for (let marker of Object.values(this.currentOverviewMarkers)) {
            marker.remove();
        }

        this.currentOverviewMarkers = {};
        this.overviewMarkerData = {};
    
    }


    /**
     * Creats a HTML button element to be used in the marker popups
     * @param personGND GND number as string
     * @param personName Name of the person
     * @returns HTML button that displays the person on click
     */
    createPersonNameButton(personGND: string, personName: string): HTMLButtonElement {

        let personNameButton = document.createElement("button");
        personNameButton.classList.add("goemap__popup_button");
        personNameButton.textContent = personName;
        personNameButton.onclick = () => {goeMapApp.setSelection([personGND])};

        return personNameButton;

    }

    /**
     * Prepares marker data that can be added to the map
     * @param personGND GND number as string for a single person
     */
    prepareMarkersForPerson(personGND: string){

        let selectedPersonData = personData["Personen"][personGND];

        if (selectedPersonData["placeOfBirth"]) {
            let coordsBirth: [number, number] = [
                selectedPersonData["placeOfBirth"]["latitude"],
                selectedPersonData["placeOfBirth"]["longitude"]
            ];

            let idCoordsBirth = `${coordsBirth[0]}${coordsBirth[1]}`;

            // Creates marker for place of birth
            if (!(idCoordsBirth in this.markerData)) {
                let markerData = new MarkerData();
                markerData.heading = selectedPersonData["placeOfBirth"]["name"];
                markerData.coordinates = coordsBirth;
                this.markerData[idCoordsBirth] = markerData;
            }

            let popupContentBirthElement = customElementManager.createCustomElement("goemap-birth-popup", {
                "person_name": this.createPersonNameButton(personGND, selectedPersonData["einsender"]),
                "place_name": selectedPersonData["placeOfBirth"]["name"]
                }
            )
            this.markerData[idCoordsBirth].containsBirth += 1;
            this.markerData[idCoordsBirth].popupContentBirths.appendChild(popupContentBirthElement);
        }


        if (selectedPersonData["placeOfDeath"]) {
            let coordsDeath: [number, number] = [
                selectedPersonData["placeOfDeath"]["latitude"],
                selectedPersonData["placeOfDeath"]["longitude"]
            ];

            let idCoordsDeath = `${coordsDeath[0]}${coordsDeath[1]}`;
            
            // Creates marker for place of death
            if (!(idCoordsDeath in this.markerData)) {
                let markerData = new MarkerData();
                markerData.heading = selectedPersonData["placeOfDeath"]["name"];
                markerData.coordinates = coordsDeath;
                this.markerData[idCoordsDeath] = markerData;
            }
            
            let popupContentDeathElement = customElementManager.createCustomElement("goemap-death-popup", {
                "person_name": this.createPersonNameButton(personGND, selectedPersonData["einsender"]),
                "place_name": selectedPersonData["placeOfDeat"["name"]]
                }
            )
            this.markerData[idCoordsDeath].containsDeath += 1;
            this.markerData[idCoordsDeath].popupContentDeaths.appendChild(popupContentDeathElement);
        }


        // Creates markers for submissions
        for (let submissionID of personData["Personen"][personGND]["Einsendungen"]) {
            
            if (!personData["Einsendungen"][submissionID]["Getty Koordinaten"]) {
                continue;
            }

            let coordsSubmission: [number, number] = [
                personData["Einsendungen"][submissionID]["Getty Koordinaten"]["latitude"],
                personData["Einsendungen"][submissionID]["Getty Koordinaten"]["longitude"]
            ];
            let idCoordsSubmission = `${coordsSubmission[0]}${coordsSubmission[1]}`;

            if (!(idCoordsSubmission in this.markerData)) {
                let markerData = new MarkerData();
                markerData.heading = personData["Einsendungen"][submissionID]["Einsendungsort"]
                markerData.coordinates = coordsSubmission;
                this.markerData[idCoordsSubmission] = markerData;
            }
            
            let slots = {
                "title": personData["Einsendungen"][submissionID]["Titel"],
                "author": personData["Einsendungen"][submissionID]["Autor"],
                "place_name": personData["Einsendungen"][submissionID]["Einsendungsort"],
                "person_name": this.createPersonNameButton(personGND, selectedPersonData["einsender"])
            }
            let popupContentElement = customElementManager.createCustomElement("goemap-submission-popup", slots);
            this.markerData[idCoordsSubmission].containsSubmissions += 1;
            this.markerData[idCoordsSubmission].popupContentSubmissions.appendChild(popupContentElement);

        }

    }

    /**
     * An overview marker shows details about a person: Portrait, 
     *  summary and a button to expand the person details in the sidebar
     * @param personGND GND number of the person
     */
    prepareOverviewMarker(personGND: string) {

        let selectedPersonData = personData["Personen"][personGND];

        if (!selectedPersonData["placeOfBirth"]) {
            return;
        }

        let coordsBirth: [number, number] = [
            selectedPersonData["placeOfBirth"]["latitude"],
            selectedPersonData["placeOfBirth"]["longitude"]
        ];

        let idCoordsBirth = `${coordsBirth[0]}${coordsBirth[1]}`;

        // Checks if a marker with the same coordinates already exists
        if (selectedPersonData["placeOfBirth"] && !(idCoordsBirth in this.overviewMarkerData)) {
            let overviewMarkerData = new MarkerData();
            overviewMarkerData.heading = selectedPersonData["placeOfBirth"]["name"];  // uses place of birth as header
            overviewMarkerData.coordinates = coordsBirth;
            this.overviewMarkerData[idCoordsBirth] = overviewMarkerData;
        }

        let summaryElement = document.createElement("p");
        // summaryElement.innerHTML = selectedPersonData["Einsender Funktion"];
        summaryElement.innerHTML = `geboren ${selectedPersonData["dateOfBirth"]}<br>`;

        if (selectedPersonData["Einsendungen"].length > 1) {
            summaryElement.innerHTML += `${selectedPersonData["Einsendungen"].length} Einsendungen`;
        } else {
            summaryElement.innerHTML += "eine Einsendung";

        }
        

        let personImage = document.createElement("img");
        if (selectedPersonData["bild_link"]) {
            personImage.setAttribute("data-imgsrc", selectedPersonData["bild_link"]);

        } else {
            personImage.setAttribute("data-imgsrc", "images/placeholder_portrait_de.png");
        }
        personImage.loading = "lazy";
        
        // Creates popup content element
        let popupContentElement = customElementManager.createCustomElement("goemap-overview-popup", {
            "person_name": this.createPersonNameButton(personGND, selectedPersonData["einsender"]),
            "person_portrait": personImage,
            "person_summary": summaryElement,
            "person_open": this.createPersonNameButton(personGND, "Steckbrief öffnen")
            }
        )
        if (this.overviewMarkerData[idCoordsBirth].overviewTooltipText) {
            this.overviewMarkerData[idCoordsBirth].overviewTooltipText += `, ${selectedPersonData["einsender"]}`
        } else {
            this.overviewMarkerData[idCoordsBirth].overviewTooltipText = selectedPersonData["einsender"];
        }
        this.overviewMarkerData[idCoordsBirth].popupContentMain.appendChild(popupContentElement);

    }


    /**
     * Adds all prepared markers to the map
     */
    addPreparedMarkersToMap(markerData: any, layer: any) {

        let addedMarkers = {};

        for (let markerID in markerData) {

            let currentMarker = markerData[markerID];
            currentMarker.createIcon();
            
            let markerOptions = {};
            if (currentMarker.icon) {
                markerOptions = {"icon": currentMarker.icon, "shadowUrl": "images/marker-shadow.png"}
            }

            let marker = L.marker(currentMarker["coordinates"], markerOptions).addTo(layer);
            let popupContentElement = document.createElement("div");
            popupContentElement.classList.add("goemap__popup_content_wrapper");
            let markerHeading = document.createElement("p");
            markerHeading.classList.add("goemap__popup_place_name");
            markerHeading.textContent = currentMarker["heading"];
            popupContentElement.appendChild(markerHeading)
            popupContentElement.appendChild(currentMarker["popupContentMain"]);
            popupContentElement.appendChild(currentMarker["popupContentBirths"]);
            popupContentElement.appendChild(currentMarker["popupContentDeaths"]);
            popupContentElement.appendChild(currentMarker["popupContentSubmissions"]);
            marker.bindPopup(popupContentElement, {
                // "keepInView": true, problems on smaller screens
                "maxHeight": 600
            });

            let tooltipText = currentMarker.overviewTooltipText;  // Either person names or empty string

            if (currentMarker.containsBirth) {
                tooltipText += `*️: ${currentMarker.containsBirth}&nbsp;&nbsp;`;
            }
            if (currentMarker.containsDeath) {
                tooltipText += `†: ${currentMarker.containsDeath}&nbsp;&nbsp;`;
            }
            if (currentMarker.containsSubmissions) {
                tooltipText += `📕: ${currentMarker.containsSubmissions}`;
            }
            

            if (tooltipText) {
                marker.bindTooltip(tooltipText);
            }

            addedMarkers[markerID] = marker;
            
        }

        return addedMarkers;

    }

    /**
     * Adds markers on the map for a selection of persons.
     * This includes place of birth, death and a marker for every place of submission.
     * @param selection Array of person GNDs
     */
    addMarkers(selection: Array<string>) {

        this.clearCurrentMarkers();

        for (let personGND of selection) {
            this.prepareMarkersForPerson(personGND);
        }

        Object.assign(this.currentMarkers, this.addPreparedMarkersToMap(this.markerData, this.leafletMap));
        this.fitMapToMarkers(Object.values(this.currentMarkers));
        
    }
    
    
    fitMapToMarkers(markers=Object.values(this.currentMarkers)) {

        let latLngs = markers.map(marker => marker.getLatLng());
        let latLngBounds = L.latLngBounds(latLngs);
        this.leafletMap.fitBounds(latLngBounds, {padding: [80, 80]}) // without animation
        // this.leafletMap.flyToBounds(latLngBounds, {padding: [80, 80]})
    
    }

    /**
     * The Overview Markers reflect the filter list in the sidebar.
     * They display the persons' birth places and on click they display a popup with their
     * portrait and a button to view their details in the middle pane.
     */
    async addOverviewMarkers(filteredGNDs: Array<string>) {
        this.clearCurrentMarkers();
        
        for (let personGND of filteredGNDs) {
            this.prepareOverviewMarker(personGND);
        }
        
        this.overviewLayer = L.layerGroup();
        Object.assign(this.currentOverviewMarkers, this.addPreparedMarkersToMap(this.overviewMarkerData, this.overviewLayer));
        this.fitMapToMarkers(Object.values(this.currentOverviewMarkers));
    }


    /**
     * Brings a map marker into focus by centering it and then opening its popup
     * @param markerID ID of the marker
     */
    focusMarker(markerID: string) {

        this.invalidateMapSize();

        if (markerID in this.currentMarkers) {
            this.leafletMap.panTo(this.currentMarkers[markerID].getLatLng());

            // Opens the popup after panning is complete
            setTimeout(() => {this.currentMarkers[markerID].openPopup();}, 250);
            
        }

    }


    /**
     * Tells the map that the dimenstions have changed.
     * This is necessary so that panning and zooming to markers works
     * 
     */
    invalidateMapSize() {

        this.leafletMap.invalidateSize();

    }


    async showOverviewLayer() {

        this.overviewLayer.addTo(this.leafletMap);
        this.fitMapToMarkers(Object.values(this.currentOverviewMarkers));

    }


    async hideOverviewLayer() {

        this.leafletMap.removeLayer(this.overviewLayer);

    }

    
}