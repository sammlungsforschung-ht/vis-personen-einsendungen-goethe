# Visualisierung der Personen hinter den Bucheinsendungen an Goethe

Dies ist der Quellcode zur Visualisierung der Personen hinter den Bucheinsendungen an Goethe.
Die Visualisierung selbst ist derzeit noch nicht öffentlich.


---

This is the source of an interactive HTML + JS visualization of some of the book submissions that Goethe received throughout his life.
It focuses on the persons who sent books to Goethe.


## Screenshots


![Start](screenshots/Screenshot_Start.png)

![Personenliste Filter](screenshots/Screenshot_Personenliste_Filter.png)

![Steckbrief](screenshots/Screenshot_Steckbrief.png)

![Personenliste Mehfachauswahl](screenshots/Screenshot_Personenliste_Mehfachauswahl.png)

![Mobil](screenshots/Screenshot_Mobil.png)
